<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Teikei
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
<div class="col-sm-3">
	<div class="sidebar">

		<strong>Categorias</strong>

		<div class="categorias">
		<?php
			$categories = get_the_category();
			$categoriaAtual = $categories[0]->cat_ID;
			// LISTA DE CATEGORIAS
			$arrayCategorias = array();
			$categorias=get_categories($args);
			foreach($categorias as $categoria):
				$nomeCategoria = $categoria->name;
				$descricao = $categoria->description;

			if ($categoriaAtual == $nomeCategoria ):
		?>
		<a class="ativo" href="<?php echo get_category_link($categoria->cat_ID); ?>"><?php echo $nomeCategoria; ?></a>
		<?php else: ?>
		<a href="<?php echo get_category_link($categoria->cat_ID); ?>"><?php echo $nomeCategoria; ?></a>
		<?php endif;endforeach; ?>
		</div>
		<strong>Recentes</strong>

		<div class="recentes">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</div>

	</div>
</div>