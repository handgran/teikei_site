<?php
/**
 * Template Name: A Empresa
 * Description: A Empresa
 *
 * @package Teikei
 */
$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$foto = $foto[0];

$empresa_icone = rwmb_meta('Teikei_empresa_icone');
$empresa_textoEstrutura = rwmb_meta('Teikei_empresa_textoEstrutura');
$galeria_imagens = rwmb_meta('Teikei_empresa_galeria_imagens');
get_header(); ?>
	<!-- PÁGINA A EMPRESA -->
	<div class="pg pg-empresa">
		
		<!-- BANNER TOPO  -->
		<figure class="bannerTopo" style="background: url( <?php echo $foto ?>)">
			<div class="container">
				<p>A Empresa</p>
			</div>
		</figure>	
	
		<div class="container">
			<div class="linkNavegacao">
				<a href="<?php echo home_url('/inicial/'); ?>"><i class="fa fa-angle-left" aria-hidden="true"></i> Voltar</a>
				<span>Home | <small>A Empresa</small></span>
			</div>
		</div>

		<div class="container">
			<section class="areaHistoria">
				<h6><?php echo get_the_title() ?></h6>
				<article>
					<p>
					<?php echo the_content() ?>
					</p>
				</article>
				<?php 
					foreach ($empresa_icone as $empresa_icone):
						$empresa_icone = $empresa_icone['full_url'];
				?>
				<img src="<?php echo $empresa_icone ?> " alt="<?php echo get_the_title() ?>">
			<?php endforeach; ?>
			</section>
		</div>

		<section class="areaEstrutura">
			<div class="container correcaoContainer">
				<div class="row">

					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="textEstrutura">
							<?php echo $empresa_textoEstrutura ?>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="carrosselImagemEstrutura">
							<div id="carrosselEstrutura" class="owl-Carousel">
								<?php 
									foreach ($galeria_imagens  as $galeria_imagens ):
										$galeria_imagens = $galeria_imagens['full_url'];
								?>
								<figure class="item" style="background:url( <?php echo $galeria_imagens ?>)"></figure>
							<?php endforeach; ?>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>

		<section class="areaEspecialista">
			<h6><?php echo $equipe_funcao = rwmb_meta('Teikei_empresa_especialista_titulo'); ?></h6>
			<p><?php echo $equipe_funcao = rwmb_meta('Teikei_empresa_especialista_sub'); ?></p>
			<div class="container">
				<ul>
					<?php 
					//LOOP DE POST EQUIPE
					$integrantes = new WP_Query( array( 'post_type' => 'equipe', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
					while ( $integrantes->have_posts() ) : $integrantes->the_post();
						$fotoIntegrante = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoIntegrante = $fotoIntegrante[0];
						$equipe_funcao = rwmb_meta('Teikei_equipe_funcao');

						?>
					<li title="<?php echo get_the_title() ?> ">
						<figure style="background:url(<?php echo $fotoIntegrante ?>)"></figure>
						<h3><?php echo get_the_title() ?></h3>
						<p><?php echo $equipe_funcao ?></p>
					</li>
					<?php endwhile; wp_reset_query(); ?>
				</ul>
			</div>
		</section>

		<div class="mapaGoogle">
			<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco'] ?>" target="_blank">
				<img src="<?php bloginfo('template_directory'); ?>/img/mapa.png" alt="Mapa" class="desk">
				<img src="<?php bloginfo('template_directory'); ?>/img/mapamobal.png" alt="Mapa" class="hiddenNone mobal">
			</a>
		</div>
	</div>
<?php get_footer(); ?>