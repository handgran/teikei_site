<?php
/**
 * Template Name: Treinamentos
 * Description: Treinamentos
 *
 * @package Teikei
 */
$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$foto = $foto[0];

$treinamentos_titulo = rwmb_meta('Teikei_treinamentos_titulo');
$treinamentos_areaCursos = rwmb_meta('Teikei_treinamentos_areaCursos');
$treinamentos_areaCursosSub = rwmb_meta('Teikei_treinamentos_areaCursosSub');

get_header(); ?>
	<!-- PÁGIANA TREINAMENTOS -->
	<div class="pg pg-treinamentos">
		
		<!-- BANNER TOPO  -->
		<figure class="bannerTopo" style="background: url(<?php echo $foto  ?> )">
			
			<div class="container">
				<p><?php echo get_the_title() ?></p>
			</div>
		</figure>

		<div class="container">
			<div class="linkNavegacao">
				<a href="<?php echo home_url('/inicial/'); ?>"><i class="fa fa-angle-left" aria-hidden="true"></i> Voltar</a>
				<span>Home | <small>Treinamentos</small></span>
			</div>
		</div>	

		<section class="areaTexto container">
			<h6>A TEIKEI dispõe de treinamentos in loco a clientes que desejam aprimorar seus conhecimentos</h6>
			<article>
				<?php echo the_content() ?>
			</article>
		</section>

		<section class="proximosCursos">
			<h6><?php echo $treinamentos_titulo ?></h6>
			<p><?php echo $treinamentos_areaCursosSub ?></p>

			

			<div class="container">
				<?php 
					//LOOP DE POST PARCEIROS
					$parceiros = new WP_Query( array( 'post_type' => 'treinamentos', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
					while ( $parceiros->have_posts() ) : $parceiros->the_post();
					$treinamento_local = rwmb_meta('Teikei_treinamento_local');
					$treinamento_link = rwmb_meta('Teikei_treinamento_link');
				 ?>
				<div class="post">
					<div class="data">
						<span><?php the_time('j') ?><?php the_time('M') ?></span>
					</div>
					<div class="row">
						<div class="col-sm-2"></div>
						<div class="col-sm-7">
							<div class="areaDescricao">
								<h2><?php echo get_the_title() ?></h2>
								<span><?php echo $treinamento_local ?></span>
								<?php echo the_content() ?>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="link">
								<a href=" <?php echo $treinamento_link ?>">Saiba mas <i class="fa fa-angle-right" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
				</div>
				<?php endwhile; wp_reset_query(); ?>

			</div>
		</section>

		<!-- ÁREA FOMRULÁRIO -->
		<section class="areaFomrularioContato">
			<h6>Precisa de um orçamento?</h6>
			<p>Preencha seus dados abaixo e em seguida entraremos em contato.</p>

			<div class="container">
				<div class="form">
					<?php echo do_shortcode('[contact-form-7 id="117" title="Formulário de Treinamentos"]'); ?>
				</div>
			</div>

		</section>

		<div class="mapaGoogle">
			<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco'] ?>" target="_blank">
				<img src="<?php bloginfo('template_directory'); ?>/img/mapa.png" alt="Mapa" class="desk">
				<img src="<?php bloginfo('template_directory'); ?>/img/mapamobal.png" alt="Mapa" class="hiddenNone mobal">
			</a>
		</div>
		
	</div>
<?php get_footer(); ?>