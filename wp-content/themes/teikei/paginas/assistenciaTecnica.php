<?php
/**
 * Template Name: Assistência Técnica
 * Description: Assistência Técnica
 *
 * @package Teikei
 */
$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$foto = $foto[0];

$assitencia_galeria = rwmb_meta('Teikei_assitencia_galeria');
$tipo_assistencia_esquerdo = rwmb_meta('Teikei_tipo_assistencia_esquerdo');
$tipo_assistencia_direito = rwmb_meta('Teikei_tipo_assistencia_direito');

get_header(); ?>
	<!-- PÁGINA ASSISTÊNCIA TÉCNICA -->
	<div class="pg pg-assistenciaTecnica">
		
		<!-- BANNER TOPO  -->
		<figure class="bannerTopo" style="background: url(<?php echo $foto ?> )">
			<div class="container">
				<p><?php echo get_the_title() ?></p>
			</div>
		</figure>	

		<div class="container">
			<div class="linkNavegacao">
				<a href="<?php echo home_url('/inicial/'); ?>"><i class="fa fa-angle-left" aria-hidden="true"></i> Voltar</a>
				<span>Home | <small>Assistência Técnica</small></span>
			</div>
		</div>

		<section class="areaTextoassistencia container correcaoContainer">
			<div class="row">
				<div class="col-sm-6">
					<article class="texto">
					<?php echo the_content() ?>
					</article>
				</div>
				<div class="col-sm-6">
					<div class="carrosselImagemAssistencia">
						<div id="carrosselAssistencia" class="owl-Carousel">
							<?php 
								foreach ($assitencia_galeria as $assitencia_galeria):
									$assitencia_galeria = $assitencia_galeria['full_url'];
							?>
							<figure class="item" style="background:url(<?php echo $assitencia_galeria ?> )"></figure>
						<?php endforeach; ?>
							
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="areaLista container correcaoContainer">
			<h6 class="text-left">Tipos de Assistência</h6>
			<div class="row">
				<div class="col-sm-6">
					<div class="lista">
						<ul>
							<?php 
								foreach ($tipo_assistencia_esquerdo as $tipo_assistencia_esquerdo):
								$tipo_assistencia_esquerdo = $tipo_assistencia_esquerdo;
							?>
							<li>
								<p><i class="fa fa-check" aria-hidden="true"></i> <?php echo $tipo_assistencia_esquerdo  ?></p>
							</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="lista">
						<ul>
							<?php 
								foreach ($tipo_assistencia_direito as $tipo_assistencia_direito):
								$tipo_assistencia_direito = $tipo_assistencia_direito;
							?>
							<li>
								<p><i class="fa fa-check" aria-hidden="true"></i> <?php echo $tipo_assistencia_direito  ?></p>
							</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
			</div>
		</section>

		<!-- ÁREA LOGOS PARCEIROS -->
		<section class="areaParceiros">
			<h6>Marcas certificadas</h6>
			<!-- CARROSSEL DE PARCEIROS -->

			<div class="container correcaoContainer">
				<div id="carrosselLogosAssistencia" class="owl-Carousel">
			
					<?php 
				//LOOP DE POST PARCEIROS
				$parceiros = new WP_Query( array( 'post_type' => 'parceiros', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
				while ( $parceiros->have_posts() ) : $parceiros->the_post();
				$fotoParceiros = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$fotoParceiros = $fotoParceiros[0];
			 ?>
				<!-- ITEM -->
				<figure class="item">
					
					<!-- LOGO PARCEIRO-->
					<img src="<?php echo $fotoParceiros ?> " alt=" <?php get_the_title() ?> " class="hvr-push">
					
				</figure>
				<?php endwhile; wp_reset_query(); ?>
					
				</div>
			</div>
		</section>

		<!-- ÁREA FOMRULÁRIO -->
		<section class="areaFomrularioContato">
			<h6>Precisa de um orçamento?</h6>
			<p>Preencha seus dados abaixo e em seguida entraremos em contato.</p>

			<div class="container">
				<div class="form">
					<?php echo do_shortcode('[contact-form-7 id="108" title="Formulário Assistência Técnica"]'); ?>
				</div>
			</div>

		</section>

		<div class="mapaGoogle">
			<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco'] ?>" target="_blank">
				<img src="<?php bloginfo('template_directory'); ?>/img/mapa.png" alt="Mapa" class="desk">
				<img src="<?php bloginfo('template_directory'); ?>/img/mapamobal.png" alt="Mapa" class="hiddenNone mobal">
			</a>
		</div>
	</div>
<?php get_footer(); ?>