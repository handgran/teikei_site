<?php
/**
 * Template Name: Contato
 * Description: Contato
 *
 * @package Teikei
 */
$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$foto = $foto[0];
get_header(); ?>
<!-- PÁGINA CONTATO -->
	<div class="pg pg-contato">
		
		<!-- BANNER TOPO  -->
		<figure class="bannerTopo" style="background: url(<?php echo $foto ?>)">
			
			<div class="container">
				<p><?php echo get_the_title() ?></p>
			</div>
		</figure>

		<div class="container">
			<div class="linkNavegacao">
				<a href="<?php echo home_url('/inicial/'); ?>"><i class="fa fa-angle-left" aria-hidden="true"></i> Voltar</a>
				<span>Home | <small>Contato</small></span>
			</div>
		</div>
		
		<section class="container correcaoContainer areaFormulario">
			
			<div class="row">
				<div class="col-sm-5">
					<div class="informacoesContato">

						<?php echo the_content() ?>

						<ul>
							<li class="telefone"><a href="tel:<?php echo $configuracao['opt_telefone'] ?>"><?php echo $configuracao['opt_telefone'] ?></a></li>
							<li class="whats"><a href="tel:<?php echo $configuracao['opt_telefone2'] ?>"><?php echo $configuracao['opt_telefone2'] ?></a></li>
							<li class="email"><a href="malito:<?php echo $configuracao['opt_Email'] ?>"><?php echo $configuracao['opt_Email'] ?></a></li>
							<li class="endereco"><a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco'] ?>"><?php echo $configuracao['opt_endereco'] ?></a></li>
						</ul>	
					</div>	
				</div>
				<div class="col-sm-6">
					<div class="contato">
						<!-- ÁREA FOMRULÁRIO -->
						<section class="areaFomrularioContato">
							<h6 class="hidden">Formulario de contato</h6>
							<div class="form">
								<?php echo do_shortcode('[contact-form-7 id="123" title="Formulário de contato página contato"]'); ?>
							</div>

						</section>
					</div>
				</div>
			</div>
			
		</section>

		<div class="mapaGoogle">
			<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco'] ?>" target="_blank">
				<img src="<?php bloginfo('template_directory'); ?>/img/mapa.png" alt="Mapa" class="desk">
				<img src="<?php bloginfo('template_directory'); ?>/img/mapamobal.png" alt="Mapa" class="hiddenNone mobal">
			</a>
		</div>
	</div>
<?php get_footer(); ?>