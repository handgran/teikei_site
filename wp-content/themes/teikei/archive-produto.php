<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Teikei
 */
$banner_blog = $configuracao['produtos_foto_banner']['url'];
$produtos_logo = $configuracao['produtos_logo']['url'];
$produtos_foto_titulo = $configuracao['produtos_foto_titulo'];
$produtos_texto_titulo = $configuracao['produtos_texto_titulo'];
$produtos_texto = $configuracao['produtos_texto'];
get_header(); ?>

<!-- PÁGINA DE PRODUTOS -->
<div class="pg pg-produtos">
	
	<!-- BANNER TOPO  -->
	<figure class="bannerTopo" style="background: url( <?php echo $banner_blog ?>)">
		
		<div class="container">
			<p><?php echo $produtos_foto_titulo ?></p>
		</div>
	</figure>

	<div class="container correcaoContainer">
		<div class="linkNavegacao">
			<a href=""><i class="fa fa-angle-left" aria-hidden="true"></i> Voltar</a>
			<span>Home | <small>Assistência Técnica</small></span>
		</div>
	</div>	

	<div class="container correcaoContainer">
		<section class="areaHistoria">
			<h6><?php echo $produtos_texto_titulo ?></h6>
			<article>
				<p><?php echo $produtos_texto  ?> </p>
			</article>
			<img src=" <?php echo $produtos_logo ?>" alt="">
		</section>
	</div>

	<!-- ÁREA DE PRODUTOS -->
	<section class="areaProdutos">
		<h6 class="hidden"><?php echo $produtos_texto_titulo ?></h6>

		<!-- SERVIÇOS -->
		<div class="areaCaroosel">
			<div class="container">

				<?php 
					//LOOP DE POST PRODUTOS
					$produtos = new WP_Query( array( 'post_type' => 'produto',  'orderby' => 'asc', 'posts_per_page' => -1 ) );
					while ( $produtos->have_posts() ) : $produtos->the_post();
						$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoPost = $fotoPost[0];

				?>	
				<a href="<?php echo get_permalink() ?>" class="item">
					<figure>
						<img src="<?php echo $fotoPost ?>" alt="<?php echo get_the_title() ?>">
					</figure>
					<h2> <?php echo get_the_title() ?> </h2>
					<p><?php echo $produto_breveDescritivo = rwmb_meta('Teikei_produto_breveDescritivo'); ?></p>
					<span>Saiba mais</span>
				</a>
				<?php endwhile; wp_reset_query(); ?>
				
			</div>
		</div>
	</section>

</div>

<?php
get_footer();
