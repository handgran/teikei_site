<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Teikei
 */
$banner_blog = $configuracao['blog_foto_banner']['url'];
$blog_foto_titulo = $configuracao['blog_foto_titulo'];
get_header(); ?>

<!-- PÁGINA BLOG -->
<div class="pg pg-blog">
	<!-- BANNER TOPO  -->
	<figure class="bannerTopo" style="background: url( <?php echo $banner_blog ?> )">
		
		<div class="container">
			<p><?php echo $blog_foto_titulo ?></p>
		</div>
	</figure>

	<div class="container">
		<div class="linkNavegacao">
			<a href="<?php echo home_url('/inicial/'); ?>"><i class="fa fa-angle-left" aria-hidden="true"></i> Voltar</a>
			<span>Home | <small>Blog</small></span>
		</div>
	</div>	

	<section class="areaBlog">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<div class="areaPost">
						<ul>
							<?php 
								if ( have_posts() ) : while( have_posts() ) : the_post();
								$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$fotoBlog = $fotoBlog[0];
								global $post;
								$categories = get_the_category();
							?>
							<li>
								<a href="<?php echo get_permalink() ?>">
									<div class="row">
										<div class="col-sm-6">
											<figure style="background:url(<?php echo $fotoBlog ?>)">
												<div class="data"><?php the_time('j') ?>  <span><?php the_time('M') ?></span></div>
											</figure>
										</div>
										<div class="col-sm-6">
											<div class="descricaoPost">
												<h2><?php echo get_the_title() ?></h2>
												<p><?php customExcerpt(200); ?></p>
												<span>Saiba mais</span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<?php endwhile;endif; wp_reset_query(); ?>

						</ul>
					</div>

					<div class="paginador">
						<ul>
							<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
						</ul>
					</div>
				</div>
				<?php  get_sidebar(); ?>
			
			</div>
		</div>
	</section>

	<div class="mapaGoogle">
			<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco'] ?>" target="_blank">
				<img src="<?php bloginfo('template_directory'); ?>/img/mapa.png" alt="Mapa" class="desk">
				<img src="<?php bloginfo('template_directory'); ?>/img/mapamobal.png" alt="Mapa" class="hiddenNone mobal">
			</a>
		</div>
</div>


<?php
get_footer();
