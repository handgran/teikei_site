<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Teikei
 */
	$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$fotoBlog = $fotoBlog[0];
	$banner_blog = $configuracao['blog_foto_banner']['url'];
	$blog_foto_titulo = $configuracao['blog_foto_titulo'];
get_header(); ?>

	<!-- PÁGINA DO POST -->
	<div class="pg pg-post">
		
		<!-- BANNER TOPO  -->
		<figure class="bannerTopo" style="background: url( <?php echo $banner_blog ?> )">
			
			<div class="container">
				<p><?php echo $blog_foto_titulo ?></p>
			</div>
		</figure>

		<div class="container">
			<div class="linkNavegacao">
				<a href="<?php echo home_url('/blog/'); ?>"><i class="fa fa-angle-left" aria-hidden="true"></i> Voltar</a>
				<span>Blog | <small> <?php echo get_the_title() ?></small></span>
			</div>
		</div>	

		<section class="areaBlog">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="areaPost">
							<ul>
								<li>
									<div class="row">
										<div class="col-sm-12">
											<figure style="background:url(<?php echo $fotoBlog ?>)">
												<div class="data"><?php the_time('j') ?>   <span><?php the_time('M') ?></span></div>
											</figure>
										</div>
										<div class="col-sm-12">
											<article class="descricaoPost">
												<h2><?php echo get_the_title() ?></h2>
												<?php echo the_content() ?>
											</article>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<div class="paginador">
							<div class="row">
								<div class="col-xs-6">
									<?php previous_post('%',' Anterior', 'no') ?>
								</div>
								<div class="col-xs-6 text-right">
									
									<?php next_post('%','	Próxima ', 'no') ?>
								</div>
							</div>
							
								
						</div>
					</div>
					<?php get_sidebar(); ?>
				</div>
			</div>
		</section>

		<div class="mapaGoogle">
			<img src="img/mapa.png" alt="">
		</div>	
	</div>

<?php

get_footer();
