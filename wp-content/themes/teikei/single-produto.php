<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Teikei
 */
	$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$foto = $foto[0];
	$banner_blog = $configuracao['produtos_foto_banner']['url'];

get_header(); ?>

	<!-- PÁGINA DO PRODUTO -->
	<div class="pg pg-produto">
		
		<!-- BANNER TOPO  -->
		<figure class="bannerTopo" style="background: url(<?php echo $banner_blog ?> )">
			
			<div class="container">
				<p><?php echo get_the_title() ?></p>
			</div>
		</figure>

		<div class="container">
			<div class="linkNavegacao">
				<a href="<?php echo home_url('/produto/'); ?>"><i class="fa fa-angle-left" aria-hidden="true"></i> Voltar</a>
				<span>Produtos | <small> <?php echo get_the_title() ?></small></span>
			</div>
		</div>	
<h6>Características</h6>
<h6>Especificações</h6>
		<section class="areaProduto">
			<div class="containerArea">
				<div class="row">
					
					<div class="col-sm-6">
						<div class="especificacoes">
							<h2><?php echo get_the_title() ?></h2>
							<span><?php echo $produto_subtitulo = rwmb_meta('Teikei_produto_subtitulo'); ?></span>

							<div class="carrosselImagemAssistencia">
								<div id="carrosselProduto" class="owl-Carousel">
									<?php 
										$produto_galeria = rwmb_meta('Teikei_produto_galeria'); 
										foreach ($produto_galeria as $produto_galeria):
											$produto_galeria = $produto_galeria['full_url'];
										 
									?>
									<figure class="item" style="background:url(<?php echo $produto_galeria  ?>)"></figure>
									 <?php endforeach; ?>
								</div>
							</div>

						</div>
					</div>

					<div class="col-sm-6">
						<div class="especificacoes">
							<article>
								<?php echo the_content() ?>
							</article>
							<a href="<?php echo $produto_arquivo = rwmb_meta('Teikei_produto_arquivo'); ?>" class="linkBaixarArquivos" target="_blank" >Baixar Descritivo Técnico</a>
							<a href="#orcamento" class="linkOrcamento scrollTop">Orçamento</a>
						</div>
					</div>

				</div>
			</div>
		</section>

		<section class="areaEspecificacoes">
			<div class="containerArea">
				<div class="row">
					<div class="col-sm-6">
						<div class="texto esquerdo">
							<?php echo $produto_galeria = rwmb_meta('Teikei_produto_caracteristica');  ?>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="texto direito">
							<?php echo $produto_galeria = rwmb_meta('Teikei_produto_especificacoes');  ?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- ÁREA FOMRULÁRIO -->
		<section class="areaFomrularioContato" id="orcamento">
			<h6><?php echo $configuracao['produtos_titulo_form'] ?></h6>
			<p><?php echo $configuracao['produtos_sub_form'] ?></p>

			<div class="container">
				<div class="form">
					<?php echo do_shortcode('[contact-form-7 id="104" title="Formulário orçamento produto"]'); ?>
				</div>
			</div>

		</section>

		<div class="mapaGoogle">
			<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco'] ?>" target="_blank">
				<img src="<?php bloginfo('template_directory'); ?>/img/mapa.png" alt="Mapa" class="desk">
				<img src="<?php bloginfo('template_directory'); ?>/img/mapamobal.png" alt="Mapa" class="hiddenNone mobal">
			</a>
		</div>
	</div>

<?php

get_footer();
