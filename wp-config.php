<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'projetos_teikei_site');


/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');


/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');


/** Nome do host do MySQL */
define('DB_HOST', 'localhost');


/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');


/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';L#&0Vt.08t3}M*vM,lx3]ql8N+)J:H(gg|V7j|AB]RA[`49?C@N>EJfkYq94P7y');

define('SECURE_AUTH_KEY',  'N9<!ErZx*mWt!Kcv LA7~ |0Z@]#2|}0KzPTW|=V{V[H)PmV8u05ZzVMR,sx93I)');

define('LOGGED_IN_KEY',    ':u<K8)EoC]x/Yuzl|zH07O8df;/jTRZ|WyZ~VW?/DXRw03ex}ZB$%-A~IkJmQ=Yy');

define('NONCE_KEY',        'qh`C_3@2d7x`#cEP?r{qi(~RPP8De7EZ_(Pb1fG:TMq@A@t}TwZLOS3r0/U=^E7p');

define('AUTH_SALT',        's2Z$1-wG-TquUVuD`ygPrlo;ky,s6?/)`RcbJbn=LSp3d+i6.2BYkQxJ#EKswK0.');

define('SECURE_AUTH_SALT', '.:ldFDUF[fPITukhO?V4d($yF7[u:(KS6RTR!kDto(j1_6k6@D;8>R7HwoEM9Pa&');

define('LOGGED_IN_SALT',   ']!O&#;7-H7eJm=Cu1K$B_$^V>#<+*B~hhnV|jdzp4sM$`A&p}!8$|Gu0$>DTpi,%');

define('NONCE_SALT',       'YUh.W8F1]-f?$Mt+R7ymzBe*W!sMK.lSEk]!@nTL]%&?5$JtfG>.)O&wWSQeI]f ');


/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'tk_';


/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
//Disable File Edits
define('DISALLOW_FILE_EDIT', true);